module gitlab.com/oraxpool/orax-cli

go 1.12

require (
	github.com/FactomProject/FactomCode v0.3.5 // indirect
	github.com/FactomProject/basen v0.0.0-20150613233007-fe3947df716e // indirect
	github.com/FactomProject/bolt v1.1.0 // indirect
	github.com/FactomProject/btcd v0.3.5 // indirect
	github.com/FactomProject/btcutil v0.0.0-20160826074221-43986820ccd5 // indirect
	github.com/FactomProject/btcutilecc v0.0.0-20130527213604-d3a63a5752ec // indirect
	github.com/FactomProject/dynrsrc v0.3.1 // indirect
	github.com/FactomProject/ed25519 v0.0.0-20150814230546-38002c4fe7b6 // indirect
	github.com/FactomProject/factoid v0.3.4 // indirect
	github.com/FactomProject/factom v0.0.0-20190712161128-5edf7247fc87
	github.com/FactomProject/factomd v6.3.3+incompatible // indirect
	github.com/FactomProject/fastsha256 v0.2.1 // indirect
	github.com/FactomProject/fsnotify v0.9.0 // indirect
	github.com/FactomProject/go-bip32 v0.3.5 // indirect
	github.com/FactomProject/go-bip39 v0.3.5 // indirect
	github.com/FactomProject/go-bip44 v0.0.0-20190306062959-b541a96d8da9 // indirect
	github.com/FactomProject/go-simplejson v0.5.0 // indirect
	github.com/FactomProject/go-spew v0.0.0-20160301052117-ddfaec9b42f5 // indirect
	github.com/FactomProject/gocoding v0.0.0-20150814232539-59666ce39524 // indirect
	github.com/FactomProject/goleveldb v0.2.1 // indirect
	github.com/FactomProject/logrustash v0.0.0-20171005151533-9c7278ede46e // indirect
	github.com/FactomProject/netki-go-partner-client v0.0.0-20160324224126-426acb535e66 // indirect
	github.com/FactomProject/serveridentity v0.0.0-20180611231115-cf42d2aa8deb // indirect
	github.com/FactomProject/web v0.1.0 // indirect
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/btcsuitereleases/btcutil v0.0.0-20150612230727-f2b1058a8255 // indirect
	github.com/cenkalti/backoff v2.1.1+incompatible
	github.com/cmars/basen v0.0.0-20150613233007-fe3947df716e // indirect
	github.com/codegangsta/cli v1.20.0 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.7.0
	github.com/go-ini/ini v1.44.0 // indirect
	github.com/google/flatbuffers v1.11.0
	github.com/gorilla/websocket v1.4.1
	github.com/goware/emailx v0.2.0
	github.com/hashicorp/go-plugin v1.0.1 // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/manifoldco/promptui v0.3.2
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.1
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/pegnet/LXR256 v0.0.0-20190721001507-5e925f415fa2 // indirect
	github.com/pegnet/LXRHash v0.0.0-20200205233914-cceb516c4b7f
	github.com/pegnet/OracleRecord v0.0.2 // indirect
	github.com/pegnet/pegnet v0.0.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.4.0
	github.com/zpatrick/go-config v0.0.0-20190509173111-460869022dbd // indirect
	gitlab.com/oraxpool/orax-message v0.0.0-20190921191632-bfac1083c89e
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
	gopkg.in/gcfg.v1 v1.2.3 // indirect
	gopkg.in/resty.v1 v1.12.0
	gopkg.in/warnings.v0 v0.1.2 // indirect
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
)
